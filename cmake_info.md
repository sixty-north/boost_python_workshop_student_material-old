# Setting up the system for Linux, MacOSX or Windows using CMake

As an alternative to the build instructions above, you can try these experimental instructions using CMake.

## Windows

* Download [Git](http://git-scm.com/download/win) and [CMake](http://www.cmake.org/files/v3.0/cmake-3.0.0-win32-x86.exe)

* Start Git Bash

```
git clone git://github.com/jcfr/scipy2014_boost_python_workshop_student_material.git workshop
mkdir build && cd $_
cmake -G "Visual Studio 10 2010" ../workshop
cmake --build ../ --config Release
```

## Linux

```
sudo apt-get install cmake
sudo apt-get build-essentials
sudo apt-get install libpython-dev
sudo apt-get libboost1.54-dev
git clone git://github.com/jcfr/scipy2014_boost_python_workshop_student_material.git workshop
mkdir build && cd $_
cmake ../workshop
make
```

## MacOSX

* Download [Git](http://git-scm.com/download/mac) and [CMake](http://www.cmake.org/files/v3.0/cmake-3.0.0-Darwin64-universal.dmg)

* Install Boost and Python

```
git clone git://github.com/jcfr/scipy2014_boost_python_workshop_student_material.git workshop
mkdir build && cd $_
cmake ../workshop
make
```

## Running tests

In all three cases, convenience tests have been added and can easily be run:
```
$ cd scipy2014_boost_python_workshop_student_material-build
$ ctest -N
Test project [...]
  Test #1: plumbing_test
  Test #2: smoke_test

Total Tests: 2

$ ctest
Test project [...]
Start 1: plumbing_test
1/2 Test #1: plumbing_test ....................   Passed    0.02 sec
    Start 2: smoke_test
2/2 Test #2: smoke_test .......................   Passed    0.02 sec

100% tests passed, 0 tests failed out of 2
```

ProTips: Adding the -V option to ctest will disp
